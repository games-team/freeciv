
 /****************************************************************************
 *                       THIS FILE WAS GENERATED                             *
 * Script: common/generate_packets.py                                        *
 * Input:  common/networking/packets.def                                     *
 *                       DO NOT CHANGE THIS FILE                             *
 ****************************************************************************/


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* common */
#include "actions.h"
#include "city.h"
#include "disaster.h"
#include "unit.h"

/* common/aicore */
#include "cm.h"

struct packet_processing_started {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_processing_finished {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_investigate_started {
  int unit_id16;
  int unit_id32;
  int city_id16;
  int city_id32;
};

struct packet_investigate_finished {
  int unit_id16;
  int unit_id32;
  int city_id16;
  int city_id32;
};

struct packet_server_join_req {
  char username[48];
  char capability[512];
  char version_label[48];
  int major_version;
  int minor_version;
  int patch_version;
};

struct packet_server_join_reply {
  bool you_can_join;
  char message[1536];
  char capability[512];
  char challenge_file[4095];
  int conn_id;
};

struct packet_authentication_req {
  enum authentication_type type;
  char message[MAX_LEN_MSG];
};

struct packet_authentication_reply {
  char password[MAX_LEN_PASSWORD];
};

struct packet_server_shutdown {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_nation_select_req {
  int player_no;
  Nation_type_id nation_no;
  bool is_male;
  char name[MAX_LEN_NAME];
  int style;
};

struct packet_player_ready {
  int player_no;
  bool is_ready;
};

struct packet_endgame_report {
  int category_num;
  char category_name[32][MAX_LEN_NAME];
  int player_num;
};

struct packet_endgame_player {
  int category_num;
  int player_id;
  int score;
  int category_score[32];
  bool winner;
};

struct packet_tile_info {
  int tile;
  Continent_id continent;
  enum known_type known;
  int owner;
  int extras_owner;
  int worked16;
  int worked32;
  Terrain_type_id terrain;
  Resource_type_id resource;
  bv_extras extras;
  int placing;
  int place_turn;
  char spec_sprite[MAX_LEN_NAME];
  char label[MAX_LEN_MAP_LABEL];
};

struct packet_game_info {
  int add_to_size_limit;
  int aifill;
  enum persistent_ready persistent_ready;
  enum airlifting_style airlifting_style;
  bool airlift_from_always_enabled;
  bool airlift_to_always_enabled;
  int angrycitizen;
  int base_pollution;
  int base_tech_cost;
  int tech_leak_pct;
  int border_city_radius_sq;
  int border_size_effect;
  int border_city_permanent_radius_sq;
  enum borders_mode borders;
  int base_bribe_cost;
  enum caravan_bonus_style caravan_bonus_style;
  int culture_vic_points;
  int culture_vic_lead;
  int culture_migration_pml;
  int history_interest_pml;
  int celebratesize;
  bool changable_tax;
  int pop_report_zeroes;
  bool citizen_nationality;
  int citizen_convert_speed;
  int conquest_convert_pct;
  int citizen_partisans_pct;
  int citymindist;
  int cooling;
  int coolinglevel;
  bv_actions diplchance_initial_odds;
  enum diplomacy_mode diplomacy;
  bool fogofwar;
  int food_cost;
  int foodbox;
  int forced_gold;
  int forced_luxury;
  int forced_science;
  int fulltradesize;
  int trade_world_rel_pct;
  int min_trade_route_val;
  enum goods_selection_method goods_selection;
  int global_advance_count;
  bool global_advances[A_LAST];
  bool global_warming;
  int globalwarming;
  int gold;
  enum gold_upkeep_style gold_upkeep_style;
  int infrapoints;
  enum revolen_type revolentype;
  Government_type_id default_government_id;
  Government_type_id government_during_revolution_id;
  int granary_food_inc;
  int granary_food_ini[MAX_GRANARY_INIS];
  int granary_num_inis;
  int great_wonder_owners[B_LAST];
  int happy_cost;
  enum happyborders_type happyborders;
  int heating;
  int illness_base_factor;
  int illness_min_size;
  bool illness_on;
  int illness_pollution_factor;
  int illness_trade_infection;
  int init_city_radius_sq;
  bool is_edit_mode;
  bool is_new_game;
  bool killcitizen;
  bool killstack;
  bool only_killing_makes_veteran;
  bool only_real_fight_makes_veteran;
  bool combat_odds_scaled_veterancy;
  bool damage_reduces_bombard_rate;
  int low_firepower_badwallattacker;
  int low_firepower_pearl_harbour;
  int low_firepower_combat_bonus;
  int low_firepower_nonnat_bombard;
  int nuke_pop_loss_pct;
  int nuke_defender_survival_chance_pct;
  int min_city_center_output[O_LAST];
  bool muuk_food_wipe;
  bool muuk_gold_wipe;
  bool muuk_shield_wipe;
  int notradesize;
  bool nuclear_winter;
  int nuclearwinter;
  int phase;
  enum phase_mode_type phase_mode;
  bool pillage_select;
  bool steal_maps_reveals_all_cities;
  bool poison_empties_food_stock;
  bool tech_steal_allow_holes;
  bool tech_trade_allow_holes;
  bool tech_trade_loss_allow_holes;
  bool tech_parasite_allow_holes;
  bool tech_loss_allow_holes;
  int rapturedelay;
  int disasters;
  bool restrictinfra;
  bool unreachable_protects;
  int sciencebox;
  int shieldbox;
  enum ai_level skill_level;
  enum victory_condition_type victory_conditions;
  bool team_pooled_research;
  int tech;
  enum tech_cost_style tech_cost_style;
  enum tech_leakage_style tech_leakage;
  int tech_upkeep_divider;
  enum tech_upkeep_style tech_upkeep_style;
  int techloss_forgiveness;
  enum free_tech_method free_tech_method;
  enum gameloss_style gameloss_style;
  int timeout;
  int first_timeout;
  bool tired_attack;
  int trademindist;
  enum trade_revenue_style trade_revenue_style;
  bool trading_city;
  bool trading_gold;
  bool trading_tech;
  int turn;
  int warminglevel;
  int year;
  bool year_0_hack;
  int fragment_count;
  bool civil_war_enabled;
  int civil_war_bonus_celebrating;
  int civil_war_bonus_unhappy;
  int granularity;
  enum wonder_visib_type small_wonder_visibility;
};

struct packet_calendar_info {
  char positive_year_label[MAX_LEN_NAME];
  char negative_year_label[MAX_LEN_NAME];
  int calendar_fragments;
  char calendar_fragment_name[MAX_CALENDAR_FRAGMENTS][MAX_LEN_NAME];
  bool calendar_skip_0;
};

struct packet_timeout_info {
  float seconds_to_phasedone;
  float last_turn_change_time;
};

struct packet_map_info {
  int xsize;
  int ysize;
  int topology_id;
};

struct packet_nuke_tile_info {
  int tile;
};

struct packet_team_name_info {
  int team_id;
  char team_name[MAX_LEN_NAME];
};

struct packet_achievement_info {
  int id;
  bool gained;
  bool first;
};

struct packet_chat_msg {
  char message[MAX_LEN_MSG];
  int tile;
  enum event_type event;
  int turn;
  int phase;
  int conn_id;
};

struct packet_early_chat_msg {
  char message[MAX_LEN_MSG];
  int tile;
  enum event_type event;
  int turn;
  int phase;
  int conn_id;
};

struct packet_chat_msg_req {
  char message[MAX_LEN_MSG];
};

struct packet_connect_msg {
  char message[MAX_LEN_MSG];
};

struct packet_server_info {
  char version_label[48];
  int major_version;
  int minor_version;
  int patch_version;
  int emerg_version;
};

struct packet_city_remove {
  int city_id16;
  int city_id32;
};

struct packet_city_info {
  int id16;
  int id32;
  int tile;
  int owner;
  int original;
  int size;
  int city_radius_sq;
  int style;
  enum capital_type capital;
  int ppl_happy[FEELING_LAST];
  int ppl_content[FEELING_LAST];
  int ppl_unhappy[FEELING_LAST];
  int ppl_angry[FEELING_LAST];
  int specialists_size;
  int specialists[SP_MAX];
  int history;
  int culture;
  int buy_cost;
  int surplus[O_LAST];
  int waste[O_LAST];
  int unhappy_penalty[O_LAST];
  int prod[O_LAST];
  int citizen_base[O_LAST];
  int usage[O_LAST];
  int food_stock;
  int shield_stock;
  int trade_route_count;
  int pollution;
  int illness_trade;
  int production_kind;
  int production_value;
  int turn_founded;
  int turn_last_built;
  int changed_from_kind;
  int changed_from_value;
  int before_change_shields;
  int disbanded_shields;
  int caravan_shields;
  int last_turns_shield_surplus;
  int airlift;
  bool did_buy;
  bool did_sell;
  bool was_happy;
  bool diplomat_investigate;
  int walls;
  int city_image;
  int steal;
  struct worklist worklist;
  bv_imprs improvements;
  bv_city_options city_options;
  char name[MAX_LEN_CITYNAME];
};

struct packet_city_nationalities {
  int id16;
  int id32;
  int nationalities_count;
  int nation_id[MAX_CITY_NATIONALITIES];
  int nation_citizens[MAX_CITY_NATIONALITIES];
};

struct packet_city_short_info {
  int id16;
  int id32;
  int tile;
  int owner;
  int size;
  int style;
  enum capital_type capital;
  bool occupied;
  int walls;
  bool happy;
  bool unhappy;
  int city_image;
  bv_imprs improvements;
  char name[MAX_LEN_CITYNAME];
};

struct packet_trade_route_info {
  int city16;
  int city32;
  int index;
  int partner16;
  int partner32;
  int value;
  enum route_direction direction;
  Goods_type_id goods;
};

struct packet_city_sell {
  int city_id16;
  int city_id32;
  int build_id;
};

struct packet_city_buy {
  int city_id16;
  int city_id32;
};

struct packet_city_change {
  int city_id16;
  int city_id32;
  int production_kind;
  int production_value;
};

struct packet_city_worklist {
  int city_id16;
  int city_id32;
  struct worklist worklist;
};

struct packet_city_make_specialist {
  int city_id16;
  int city_id32;
  int tile_id;
};

struct packet_city_make_worker {
  int city_id16;
  int city_id32;
  int tile_id;
};

struct packet_city_change_specialist {
  int city_id16;
  int city_id32;
  Specialist_type_id from;
  Specialist_type_id to;
};

struct packet_city_rename {
  int city_id16;
  int city_id32;
  char name[MAX_LEN_CITYNAME];
};

struct packet_city_options_req {
  int city_id16;
  int city_id32;
  bv_city_options options;
};

struct packet_city_refresh {
  int city_id16;
  int city_id32;
};

struct packet_city_name_suggestion_req {
  int unit_id16;
  int unit_id32;
};

struct packet_city_name_suggestion_info {
  int unit_id16;
  int unit_id32;
  char name[MAX_LEN_CITYNAME];
};

struct packet_city_sabotage_list {
  int actor_id16;
  int actor_id32;
  int city_id16;
  int city_id32;
  bv_imprs improvements;
  action_id act_id;
  int request_kind;
};

struct packet_city_rally_point {
  int city_id16;
  int city_id32;
  int length;
  bool persistent;
  bool vigilant;
  struct unit_order orders[MAX_LEN_ROUTE];
};

struct packet_worker_task {
  int city_id16;
  int city_id32;
  int tile_id;
  enum unit_activity activity;
  int tgt;
  int want;
};

struct packet_player_remove {
  int playerno;
};

struct packet_player_info {
  int playerno;
  char name[MAX_LEN_NAME];
  char username[MAX_LEN_NAME];
  bool unassigned_user;
  int score;
  bool is_male;
  bool was_created;
  Government_type_id government;
  Government_type_id target_government;
  bool real_embassy[MAX_NUM_PLAYER_SLOTS];
  enum mood_type mood;
  int style;
  int music_style;
  Nation_type_id nation;
  int team;
  bool is_ready;
  bool phase_done;
  int nturns_idle;
  int turns_alive;
  bool is_alive;
  int gold;
  int tax;
  int science;
  int luxury;
  int infrapoints;
  int tech_upkeep;
  int science_cost;
  bool is_connected;
  int revolution_finishes;
  enum ai_level ai_skill_level;
  enum barbarian_type barbarian_type;
  bv_player gives_shared_vision;
  int history;
  int culture;
  int love[MAX_NUM_PLAYER_SLOTS];
  bool color_valid;
  bool color_changeable;
  int color_red;
  int color_green;
  int color_blue;
  bv_plr_flags flags;
  int wonders[B_LAST];
  int multip_count;
  int multiplier[MAX_NUM_MULTIPLIERS];
  int multiplier_target[MAX_NUM_MULTIPLIERS];
  int multiplier_changed[MAX_NUM_MULTIPLIERS];
};

struct packet_player_phase_done {
  int turn;
};

struct packet_player_rates {
  int tax;
  int luxury;
  int science;
};

struct packet_player_change_government {
  Government_type_id government;
};

struct packet_player_place_infra {
  int tile;
  int extra;
};

struct packet_player_attribute_block {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_player_attribute_chunk {
  int offset;
  int total_length;
  int chunk_length;
  unsigned char data[ATTRIBUTE_CHUNK_SIZE];
};

struct packet_player_diplstate {
  int diplstate_id;
  int plr1;
  int plr2;
  enum diplstate_type type;
  int turns_left;
  int has_reason_to_cancel;
  int contact_turns_left;
};

struct packet_player_multiplier {
  int count;
  int multipliers[MAX_NUM_MULTIPLIERS];
};

struct packet_research_info {
  int id;
  int techs_researched;
  int future_tech;
  int researching;
  int researching_cost;
  int bulbs_researched;
  int tech_goal;
  int total_bulbs_prod;
  char inventions[A_LAST + 1];
};

struct packet_unknown_research {
  int id;
};

struct packet_player_research {
  int tech;
};

struct packet_player_tech_goal {
  int tech;
};

struct packet_unit_remove {
  int unit_id16;
  int unit_id32;
};

struct packet_unit_info {
  int id16;
  int id32;
  int owner;
  int nationality;
  int tile;
  enum direction8 facing;
  int homecity16;
  int homecity32;
  int upkeep[O_LAST];
  int veteran;
  enum server_side_agent ssa_controller;
  bool paradropped;
  bool occupied;
  bool transported;
  bool done_moving;
  bool stay;
  Unit_type_id type;
  int transported_by16;
  int transported_by32;
  int carrying;
  int movesleft;
  int hp;
  int fuel;
  int activity_count;
  int changed_from_count;
  int goto_tile;
  enum unit_activity activity;
  int activity_tgt;
  enum unit_activity changed_from;
  int changed_from_tgt;
  int battlegroup;
  bool has_orders;
  int orders_length;
  int orders_index;
  bool orders_repeat;
  bool orders_vigilant;
  struct unit_order orders[MAX_LEN_ROUTE];
  enum action_decision action_decision_want;
  int action_decision_tile;
};

struct packet_unit_short_info {
  int id16;
  int id32;
  int owner;
  int tile;
  enum direction8 facing;
  Unit_type_id type;
  int veteran;
  bool occupied;
  bool transported;
  int hp;
  int activity;
  int activity_tgt;
  int transported_by16;
  int transported_by32;
  int packet_use;
  int info_city_id16;
  int info_city_id32;
};

struct packet_unit_combat_info {
  int attacker_unit_id16;
  int attacker_unit_id32;
  int defender_unit_id16;
  int defender_unit_id32;
  int attacker_hp;
  int defender_hp;
  bool make_att_veteran;
  bool make_def_veteran;
};

struct packet_unit_sscs_set {
  int unit_id16;
  int unit_id32;
  enum unit_ss_data_type type;
  int value;
};

struct packet_unit_orders {
  int unit_id16;
  int unit_id32;
  int src_tile;
  int length;
  bool repeat;
  bool vigilant;
  struct unit_order orders[MAX_LEN_ROUTE];
  int dest_tile;
};

struct packet_unit_server_side_agent_set {
  int unit_id16;
  int unit_id32;
  enum server_side_agent agent;
};

struct packet_unit_action_query {
  int actor_id16;
  int actor_id32;
  int target_id;
  action_id action_type;
  int request_kind;
};

struct packet_unit_type_upgrade {
  Unit_type_id type;
};

struct packet_unit_do_action {
  int actor_id16;
  int actor_id32;
  int target_id;
  int sub_tgt_id;
  char name[MAX_LEN_NAME];
  action_id action_type;
};

struct packet_unit_action_answer {
  int actor_id16;
  int actor_id32;
  int target_id;
  int cost;
  action_id action_type;
  int request_kind;
};

struct packet_unit_get_actions {
  int actor_unit_id16;
  int actor_unit_id32;
  int target_unit_id16;
  int target_unit_id32;
  int target_tile_id;
  int target_extra_id;
  int request_kind;
};

struct packet_unit_actions {
  int actor_unit_id16;
  int actor_unit_id32;
  int target_unit_id16;
  int target_unit_id32;
  int target_city_id16;
  int target_city_id32;
  int target_tile_id;
  int target_extra_id;
  int request_kind;
  struct act_prob action_probabilities[MAX_NUM_ACTIONS];
};

struct packet_unit_change_activity {
  int unit_id16;
  int unit_id32;
  enum unit_activity activity;
  int target;
};

struct packet_diplomacy_init_meeting_req {
  int counterpart;
};

struct packet_diplomacy_init_meeting {
  int counterpart;
  int initiated_from;
};

struct packet_diplomacy_cancel_meeting_req {
  int counterpart;
};

struct packet_diplomacy_cancel_meeting {
  int counterpart;
  int initiated_from;
};

struct packet_diplomacy_create_clause_req {
  int counterpart;
  int giver;
  enum clause_type type;
  int value;
};

struct packet_diplomacy_create_clause {
  int counterpart;
  int giver;
  enum clause_type type;
  int value;
};

struct packet_diplomacy_remove_clause_req {
  int counterpart;
  int giver;
  enum clause_type type;
  int value;
};

struct packet_diplomacy_remove_clause {
  int counterpart;
  int giver;
  enum clause_type type;
  int value;
};

struct packet_diplomacy_accept_treaty_req {
  int counterpart;
};

struct packet_diplomacy_accept_treaty {
  int counterpart;
  bool I_accepted;
  bool other_accepted;
};

struct packet_diplomacy_cancel_pact {
  int other_player_id;
  enum clause_type clause;
};

struct packet_page_msg {
  char caption[MAX_LEN_MSG];
  char headline[MAX_LEN_MSG];
  enum event_type event;
  int len;
  int parts;
};

struct packet_page_msg_part {
  char lines[MAX_LEN_CONTENT];
};

struct packet_report_req {
  enum report_type type;
};

struct packet_conn_info {
  int id;
  bool used;
  bool established;
  bool observer;
  int player_num;
  enum cmdlevel access_level;
  char username[MAX_LEN_NAME];
  char addr[MAX_LEN_ADDR];
  char capability[MAX_LEN_CAPSTR];
};

struct packet_conn_ping_info {
  int connections;
  int conn_id[MAX_NUM_CONNECTIONS];
  float ping_time[MAX_NUM_CONNECTIONS];
};

struct packet_conn_ping {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_conn_pong {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_client_heartbeat {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_client_info {
  enum gui_type gui;
  int emerg_version;
  char distribution[MAX_LEN_NAME];
};

struct packet_end_phase {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_start_phase {
  int phase;
};

struct packet_new_year {
  int year;
  int fragments;
  int turn;
};

struct packet_begin_turn {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_end_turn {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_freeze_client {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_thaw_client {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_spaceship_launch {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_spaceship_place {
  enum spaceship_place_type type;
  int num;
};

struct packet_spaceship_info {
  int player_num;
  int sship_state;
  int structurals;
  int components;
  int modules;
  int fuel;
  int propulsion;
  int habitation;
  int life_support;
  int solar_panels;
  int launch_year;
  int population;
  int mass;
  bv_spaceship_structure structure;
  float support_rate;
  float energy_rate;
  float success_rate;
  float travel_time;
};

struct packet_ruleset_unit {
  Unit_type_id id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  char sound_move[MAX_LEN_NAME];
  char sound_move_alt[MAX_LEN_NAME];
  char sound_fight[MAX_LEN_NAME];
  char sound_fight_alt[MAX_LEN_NAME];
  int unit_class_id;
  int build_cost;
  int pop_cost;
  int attack_strength;
  int defense_strength;
  int move_rate;
  int tech_requirement;
  int build_reqs_count;
  struct requirement build_reqs[MAX_NUM_REQS];
  int vision_radius_sq;
  int transport_capacity;
  int hp;
  int firepower;
  int obsoleted_by;
  int converted_to;
  int convert_time;
  int fuel;
  int happy_cost;
  int upkeep[O_LAST];
  int paratroopers_range;
  int veteran_levels;
  char veteran_name[MAX_VET_LEVELS][MAX_LEN_NAME];
  int power_fact[MAX_VET_LEVELS];
  int move_bonus[MAX_VET_LEVELS];
  int base_raise_chance[MAX_VET_LEVELS];
  int work_raise_chance[MAX_VET_LEVELS];
  int bombard_rate;
  int city_size;
  int city_slots;
  enum transp_def_type tp_defense;
  bv_unit_classes cargo;
  bv_unit_classes targets;
  bv_unit_classes embarks;
  bv_unit_classes disembarks;
  enum vision_layer vlayer;
  char helptext[MAX_LEN_PACKET];
  bv_unit_type_flags flags;
  bv_unit_type_roles roles;
  bool worker;
};

struct packet_ruleset_unit_bonus {
  Unit_type_id unit;
  enum unit_type_flag_id flag;
  enum combat_bonus_type type;
  int value;
  bool quiet;
};

struct packet_ruleset_unit_flag {
  int id;
  char name[MAX_LEN_NAME];
  char helptxt[MAX_LEN_PACKET];
};

struct packet_ruleset_unit_class_flag {
  int id;
  char name[MAX_LEN_NAME];
  char helptxt[MAX_LEN_PACKET];
};

struct packet_ruleset_game {
  int default_specialist;
  int global_init_techs_count;
  int global_init_techs[MAX_NUM_TECH_LIST];
  int global_init_buildings_count;
  Impr_type_id global_init_buildings[MAX_NUM_BUILDING_LIST];
  int veteran_levels;
  char veteran_name[MAX_VET_LEVELS][MAX_LEN_NAME];
  int power_fact[MAX_VET_LEVELS];
  int move_bonus[MAX_VET_LEVELS];
  int base_raise_chance[MAX_VET_LEVELS];
  int work_raise_chance[MAX_VET_LEVELS];
  int background_red;
  int background_green;
  int background_blue;
};

struct packet_ruleset_specialist {
  Specialist_type_id id;
  char plural_name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char short_name[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_government_ruler_title {
  Government_type_id gov;
  Nation_type_id nation;
  char male_title[MAX_LEN_NAME];
  char female_title[MAX_LEN_NAME];
};

struct packet_ruleset_tech {
  int id;
  int root_req;
  int research_reqs_count;
  struct requirement research_reqs[MAX_NUM_REQS];
  int tclass;
  bool removed;
  bv_tech_flags flags;
  float cost;
  int num_reqs;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char helptext[MAX_LEN_PACKET];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
};

struct packet_ruleset_tech_class {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  int cost_pct;
};

struct packet_ruleset_tech_flag {
  int id;
  char name[MAX_LEN_NAME];
  char helptxt[MAX_LEN_PACKET];
};

struct packet_ruleset_government {
  Government_type_id id;
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_terrain_control {
  int ocean_reclaim_requirement_pct;
  int land_channel_requirement_pct;
  int terrain_thaw_requirement_pct;
  int terrain_freeze_requirement_pct;
  int lake_max_size;
  int min_start_native_area;
  int move_fragments;
  int igter_cost;
  bool pythagorean_diagonal;
  bool infrapoints;
  char gui_type_base0[MAX_LEN_NAME];
  char gui_type_base1[MAX_LEN_NAME];
};

struct packet_rulesets_ready {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_ruleset_nation_sets {
  int nsets;
  char names[MAX_NUM_NATION_SETS][MAX_LEN_NAME];
  char rule_names[MAX_NUM_NATION_SETS][MAX_LEN_NAME];
  char descriptions[MAX_NUM_NATION_SETS][MAX_LEN_MSG];
};

struct packet_ruleset_nation_groups {
  int ngroups;
  char groups[MAX_NUM_NATION_GROUPS][MAX_LEN_NAME];
  bool hidden[MAX_NUM_NATION_GROUPS];
};

struct packet_ruleset_nation {
  Nation_type_id id;
  char translation_domain[MAX_LEN_NAME];
  char adjective[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char noun_plural[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  char legend[MAX_LEN_MSG];
  int style;
  int leader_count;
  char leader_name[MAX_NUM_LEADERS][MAX_LEN_NAME];
  bool leader_is_male[MAX_NUM_LEADERS];
  bool is_playable;
  enum barbarian_type barbarian_type;
  int nsets;
  int sets[MAX_NUM_NATION_SETS];
  int ngroups;
  int groups[MAX_NUM_NATION_GROUPS];
  Government_type_id init_government_id;
  int init_techs_count;
  int init_techs[MAX_NUM_TECH_LIST];
  int init_units_count;
  Unit_type_id init_units[MAX_NUM_UNIT_LIST];
  int init_buildings_count;
  Impr_type_id init_buildings[MAX_NUM_BUILDING_LIST];
};

struct packet_nation_availability {
  int ncount;
  bool is_pickable[MAX_NUM_NATIONS];
  bool nationset_change;
};

struct packet_ruleset_style {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
};

struct packet_ruleset_city {
  int style_id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char citizens_graphic[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  char graphic[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
};

struct packet_ruleset_building {
  Impr_type_id id;
  enum impr_genus_id genus;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  int obs_count;
  struct requirement obs_reqs[MAX_NUM_REQS];
  int build_cost;
  int upkeep;
  int sabotage;
  bv_impr_flags flags;
  char soundtag[MAX_LEN_NAME];
  char soundtag_alt[MAX_LEN_NAME];
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_terrain {
  Terrain_type_id id;
  int tclass;
  bv_terrain_flags flags;
  bv_unit_classes native_to;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  int movement_cost;
  int defense_bonus;
  int output[O_LAST];
  int num_resources;
  Resource_type_id resources[MAX_RESOURCE_TYPES];
  int road_output_incr_pct[O_LAST];
  int base_time;
  int road_time;
  Terrain_type_id cultivate_result;
  int cultivate_time;
  Terrain_type_id plant_result;
  int plant_time;
  int irrigation_food_incr;
  int irrigation_time;
  int mining_shield_incr;
  int mining_time;
  int animal;
  Terrain_type_id transform_result;
  int transform_time;
  int placing_time;
  int clean_pollution_time;
  int clean_fallout_time;
  int pillage_time;
  int color_red;
  int color_green;
  int color_blue;
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_terrain_flag {
  int id;
  char name[MAX_LEN_NAME];
  char helptxt[MAX_LEN_PACKET];
};

struct packet_ruleset_unit_class {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  int min_speed;
  int hp_loss_pct;
  int non_native_def_pct;
  bv_unit_class_flags flags;
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_extra {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  int category;
  bv_causes causes;
  bv_rmcauses rmcauses;
  char activity_gfx[MAX_LEN_NAME];
  char act_gfx_alt[MAX_LEN_NAME];
  char act_gfx_alt2[MAX_LEN_NAME];
  char rmact_gfx[MAX_LEN_NAME];
  char rmact_gfx_alt[MAX_LEN_NAME];
  char graphic_str[MAX_LEN_NAME];
  char graphic_alt[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  int rmreqs_count;
  struct requirement rmreqs[MAX_NUM_REQS];
  int appearance_chance;
  int appearance_reqs_count;
  struct requirement appearance_reqs[MAX_NUM_REQS];
  int disappearance_chance;
  int disappearance_reqs_count;
  struct requirement disappearance_reqs[MAX_NUM_REQS];
  int visibility_req;
  bool buildable;
  bool generated;
  int build_time;
  int build_time_factor;
  int removal_time;
  int removal_time_factor;
  int infracost;
  int defense_bonus;
  enum extra_unit_seen_type eus;
  bv_unit_classes native_to;
  bv_extra_flags flags;
  bv_extras hidden_by;
  bv_extras bridged_over;
  bv_extras conflicts;
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_extra_flag {
  int id;
  char name[MAX_LEN_NAME];
  char helptxt[MAX_LEN_PACKET];
};

struct packet_ruleset_base {
  int id;
  enum base_gui_type gui_type;
  int border_sq;
  int vision_main_sq;
  int vision_invis_sq;
  int vision_subs_sq;
};

struct packet_ruleset_road {
  int id;
  enum road_gui_type gui_type;
  int first_reqs_count;
  struct requirement first_reqs[MAX_NUM_REQS];
  int move_cost;
  enum road_move_mode move_mode;
  int tile_incr_const[O_LAST];
  int tile_incr[O_LAST];
  int tile_bonus[O_LAST];
  enum road_compat compat;
  bv_max_extras integrates;
  bv_road_flags flags;
};

struct packet_ruleset_goods {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  int from_pct;
  int to_pct;
  int onetime_pct;
  bv_goods_flags flags;
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_disaster {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  int frequency;
  bv_disaster_effects effects;
};

struct packet_ruleset_achievement {
  int id;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  enum achievement_type type;
  bool unique;
  int value;
};

struct packet_ruleset_trade {
  int id;
  int trade_pct;
  enum trade_route_illegal_cancelling cancelling;
  enum trade_route_bonus_type bonus_type;
};

struct packet_ruleset_action {
  action_id id;
  char ui_name[MAX_LEN_NAME];
  bool quiet;
  enum action_result result;
  bv_action_sub_results sub_results;
  bool actor_consuming_always;
  enum action_actor_kind act_kind;
  enum action_target_kind tgt_kind;
  enum action_sub_target_kind sub_tgt_kind;
  int min_distance;
  int max_distance;
  bv_actions blocked_by;
};

struct packet_ruleset_action_enabler {
  action_id enabled_action;
  int actor_reqs_count;
  struct requirement actor_reqs[MAX_NUM_REQS];
  int target_reqs_count;
  struct requirement target_reqs[MAX_NUM_REQS];
};

struct packet_ruleset_action_auto {
  int id;
  enum action_auto_perf_cause cause;
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  int alternatives_count;
  action_id alternatives[MAX_NUM_ACTIONS];
};

struct packet_ruleset_music {
  int id;
  char music_peaceful[MAX_LEN_NAME];
  char music_combat[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
};

struct packet_ruleset_multiplier {
  Multiplier_type_id id;
  int start;
  int stop;
  int step;
  int def;
  int offset;
  int factor;
  int minimum_turns;
  char name[MAX_LEN_NAME];
  char rule_name[MAX_LEN_NAME];
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
  char helptext[MAX_LEN_PACKET];
};

struct packet_ruleset_clause {
  enum clause_type type;
  bool enabled;
  int giver_reqs_count;
  struct requirement giver_reqs[MAX_NUM_REQS];
  int receiver_reqs_count;
  struct requirement receiver_reqs[MAX_NUM_REQS];
};

struct packet_ruleset_control {
  int num_unit_classes;
  int num_unit_types;
  int num_impr_types;
  int num_tech_classes;
  int num_tech_types;
  int num_extra_types;
  int num_base_types;
  int num_road_types;
  int num_resource_types;
  int num_goods_types;
  int num_disaster_types;
  int num_achievement_types;
  int num_multipliers;
  int num_styles;
  int num_music_styles;
  int government_count;
  int nation_count;
  int styles_count;
  int terrain_count;
  int num_specialist_types;
  int num_nation_groups;
  int num_nation_sets;
  char preferred_tileset[MAX_LEN_NAME];
  char preferred_soundset[MAX_LEN_NAME];
  char preferred_musicset[MAX_LEN_NAME];
  bool popup_tech_help;
  char name[MAX_LEN_NAME];
  char version[MAX_LEN_NAME];
  char alt_dir[MAX_LEN_NAME];
  int desc_length16;
  int desc_length32;
};

struct packet_ruleset_summary {
  char text[MAX_LEN_CONTENT];
};

struct packet_ruleset_description_part {
  char text[MAX_LEN_CONTENT];
};

struct packet_single_want_hack_req {
  char token[MAX_LEN_NAME];
};

struct packet_single_want_hack_reply {
  bool you_have_hack;
};

struct packet_ruleset_choices {
  int ruleset_count;
  char rulesets[MAX_NUM_RULESETS][MAX_RULESET_NAME_LENGTH];
};

struct packet_game_load {
  bool load_successful;
  char load_filename[MAX_LEN_PACKET];
};

struct packet_server_setting_control {
  int settings_num;
  int categories_num;
  char category_names[256][MAX_LEN_NAME];
};

struct packet_server_setting_const {
  int id;
  char name[MAX_LEN_NAME];
  char short_help[MAX_LEN_PACKET];
  char extra_help[MAX_LEN_PACKET];
  int category;
};

struct packet_server_setting_bool {
  int id;
  bool is_visible;
  bool is_changeable;
  bool initial_setting;
  enum setting_default_level setdef;
  bool val;
  bool default_val;
};

struct packet_server_setting_int {
  int id;
  bool is_visible;
  bool is_changeable;
  bool initial_setting;
  enum setting_default_level setdef;
  int val;
  int default_val;
  int min_val;
  int max_val;
};

struct packet_server_setting_str {
  int id;
  bool is_visible;
  bool is_changeable;
  bool initial_setting;
  enum setting_default_level setdef;
  char val[MAX_LEN_PACKET];
  char default_val[MAX_LEN_PACKET];
};

struct packet_server_setting_enum {
  int id;
  bool is_visible;
  bool is_changeable;
  bool initial_setting;
  enum setting_default_level setdef;
  int val;
  int default_val;
  int values_num;
  char support_names[64][MAX_LEN_NAME];
  char pretty_names[64][MAX_LEN_ENUM];
};

struct packet_server_setting_bitwise {
  int id;
  bool is_visible;
  bool is_changeable;
  bool initial_setting;
  enum setting_default_level setdef;
  int val;
  int default_val;
  int bits_num;
  char support_names[64][MAX_LEN_NAME];
  char pretty_names[64][MAX_LEN_ENUM];
};

struct packet_set_topology {
  int topology_id;
};

struct packet_ruleset_effect {
  enum effect_type effect_type;
  int effect_value;
  bool has_multiplier;
  Multiplier_type_id multiplier;
  int reqs_count;
  struct requirement reqs[MAX_NUM_REQS];
};

struct packet_ruleset_resource {
  int id;
  int output[O_LAST];
};

struct packet_scenario_info {
  bool is_scenario;
  char name[256];
  char authors[MAX_LEN_PACKET / 3];
  bool players;
  bool startpos_nations;
  bool save_random;
  bool prevent_new_cities;
  bool lake_flooding;
  bool handmade;
  bool allow_ai_type_fallback;
  bool ruleset_locked;
  char datafile[MAX_LEN_NAME];
  bool have_resources;
  char req_caps[257];
};

struct packet_scenario_description {
  char description[MAX_LEN_CONTENT];
};

struct packet_save_scenario {
  char name[MAX_LEN_NAME];
};

struct packet_vote_new {
  int vote_no;
  char user[MAX_LEN_NAME];
  char desc[512];
  int percent_required;
  int flags;
};

struct packet_vote_update {
  int vote_no;
  int yes;
  int no;
  int abstain;
  int num_voters;
};

struct packet_vote_remove {
  int vote_no;
};

struct packet_vote_resolve {
  int vote_no;
  bool passed;
};

struct packet_vote_submit {
  int vote_no;
  int value;
};

struct packet_edit_mode {
  bool state;
};

struct packet_edit_recalculate_borders {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_edit_check_tiles {
  char __dummy;			/* to avoid malloc(0); */
};

struct packet_edit_toggle_fogofwar {
  int player;
};

struct packet_edit_tile_terrain {
  int tile;
  Terrain_type_id terrain;
  int size;
};

struct packet_edit_tile_extra {
  int tile;
  int extra_type_id;
  bool removal;
  int eowner;
  int size;
};

struct packet_edit_startpos {
  int id;
  bool removal;
  int tag;
};

struct packet_edit_startpos_full {
  int id;
  bool exclude;
  bv_startpos_nations nations;
};

struct packet_edit_tile {
  int tile;
  bv_extras extras;
  Resource_type_id resource;
  Terrain_type_id terrain;
  Nation_type_id startpos_nation;
  int eowner;
  char label[MAX_LEN_NAME];
};

struct packet_edit_unit_create {
  int owner;
  int tile;
  Unit_type_id type;
  int count;
  int tag;
};

struct packet_edit_unit_remove {
  int owner;
  int tile;
  Unit_type_id type;
  int count;
};

struct packet_edit_unit_remove_by_id {
  int id16;
  int id32;
};

struct packet_edit_unit {
  int id16;
  int id32;
  Unit_type_id utype;
  int owner;
  int homecity16;
  int homecity32;
  int moves_left;
  int hp;
  int veteran;
  int fuel;
  enum unit_activity activity;
  int activity_count;
  Base_type_id activity_base;
  bool debug;
  bool moved;
  bool paradropped;
  bool done_moving;
  int transported_by16;
  int transported_by32;
  bool stay;
};

struct packet_edit_city_create {
  int owner;
  int tile;
  int size;
  int tag;
};

struct packet_edit_city_remove {
  int id16;
  int id32;
};

struct packet_edit_city {
  int id16;
  int id32;
  char name[MAX_LEN_CITYNAME];
  int owner;
  int original;
  int size;
  int history;
  int ppl_happy[5];
  int ppl_content[5];
  int ppl_unhappy[5];
  int ppl_angry[5];
  int specialists_size;
  int specialists[SP_MAX];
  int food_stock;
  int shield_stock;
  bool airlift;
  bool debug;
  bool did_buy;
  bool did_sell;
  bool was_happy;
  int anarchy;
  int rapture;
  int steal;
  int turn_founded;
  int turn_last_built;
  int built[B_LAST];
  int production_kind;
  int production_value;
  bv_city_options city_options;
};

struct packet_edit_player_create {
  int tag;
};

struct packet_edit_player_remove {
  int id;
};

struct packet_edit_player {
  int id;
  char name[MAX_LEN_NAME];
  char username[MAX_LEN_NAME];
  char ranked_username[MAX_LEN_NAME];
  int user_turns;
  bool is_male;
  Government_type_id government;
  Government_type_id target_government;
  Nation_type_id nation;
  int team;
  bool phase_done;
  int nturns_idle;
  bool is_alive;
  int revolution_finishes;
  bv_player embassy;
  int gold;
  int tax;
  int science;
  int luxury;
  int future_tech;
  int researching;
  int bulbs_researched;
  bool inventions[A_LAST + 1];
  bool ai;
  bool scenario_reserved;
};

struct packet_edit_player_vision {
  int player;
  int tile;
  bool known;
  int size;
};

struct packet_edit_game {
  bool scenario;
  char scenario_name[256];
  char scenario_authors[MAX_LEN_PACKET / 3];
  bool scenario_random;
  bool scenario_players;
  bool startpos_nations;
  bool prevent_new_cities;
  bool lake_flooding;
  bool ruleset_locked;
};

struct packet_edit_scenario_desc {
  char scenario_desc[MAX_LEN_CONTENT];
};

struct packet_edit_object_created {
  int tag;
  int id;
};

struct packet_play_music {
  char tag[MAX_LEN_NAME];
};

struct packet_web_city_info_addition {
  int id16;
  int id32;
  int granary_size;
  int granary_turns;
};

struct packet_web_player_info_addition {
  int playerno;
  int expected_income;
};

struct packet_web_ruleset_unit_addition {
  Unit_type_id id;
  bv_actions utype_actions;
};

enum packet_type {
  PACKET_PROCESSING_STARTED,             /* 0 */
  PACKET_PROCESSING_FINISHED,
  PACKET_SERVER_JOIN_REQ = 4,
  PACKET_SERVER_JOIN_REPLY,
  PACKET_AUTHENTICATION_REQ,
  PACKET_AUTHENTICATION_REPLY,
  PACKET_SERVER_SHUTDOWN,
  PACKET_RULESET_TECH_CLASS,
  PACKET_NATION_SELECT_REQ,              /* 10 */
  PACKET_PLAYER_READY,
  PACKET_ENDGAME_REPORT,
  PACKET_SCENARIO_DESCRIPTION,
  PACKET_EDIT_SCENARIO_DESC,
  PACKET_TILE_INFO,
  PACKET_GAME_INFO,
  PACKET_MAP_INFO,
  PACKET_NUKE_TILE_INFO,
  PACKET_TEAM_NAME_INFO,
  PACKET_INVESTIGATE_STARTED = 21,
  PACKET_INVESTIGATE_FINISHED,
  PACKET_CHAT_MSG = 25,
  PACKET_CHAT_MSG_REQ,
  PACKET_CONNECT_MSG,
  PACKET_EARLY_CHAT_MSG,
  PACKET_SERVER_INFO,
  PACKET_CITY_REMOVE,                    /* 30 */
  PACKET_CITY_INFO,
  PACKET_CITY_SHORT_INFO,
  PACKET_CITY_SELL,
  PACKET_CITY_BUY,
  PACKET_CITY_CHANGE,
  PACKET_CITY_WORKLIST,
  PACKET_CITY_MAKE_SPECIALIST,
  PACKET_CITY_MAKE_WORKER,
  PACKET_CITY_CHANGE_SPECIALIST,
  PACKET_CITY_RENAME,                    /* 40 */
  PACKET_CITY_OPTIONS_REQ,
  PACKET_CITY_REFRESH,
  PACKET_CITY_NAME_SUGGESTION_REQ,
  PACKET_CITY_NAME_SUGGESTION_INFO,
  PACKET_CITY_SABOTAGE_LIST,
  PACKET_CITY_NATIONALITIES,
  PACKET_PLAYER_REMOVE = 50,             /* 50 */
  PACKET_PLAYER_INFO,
  PACKET_PLAYER_PHASE_DONE,
  PACKET_PLAYER_RATES,
  PACKET_PLAYER_CHANGE_GOVERNMENT,
  PACKET_PLAYER_RESEARCH,
  PACKET_PLAYER_TECH_GOAL,
  PACKET_PLAYER_ATTRIBUTE_BLOCK,
  PACKET_PLAYER_ATTRIBUTE_CHUNK,
  PACKET_PLAYER_DIPLSTATE,
  PACKET_RESEARCH_INFO,                  /* 60 */
  PACKET_PLAYER_PLACE_INFRA,
  PACKET_UNIT_REMOVE,
  PACKET_UNIT_INFO,
  PACKET_UNIT_SHORT_INFO,
  PACKET_UNIT_COMBAT_INFO,
  PACKET_UNKNOWN_RESEARCH,
  PACKET_UNIT_SSCS_SET = 71,
  PACKET_UNIT_ORDERS = 73,
  PACKET_UNIT_SERVER_SIDE_AGENT_SET,
  PACKET_UNIT_ACTION_QUERY = 82,
  PACKET_UNIT_TYPE_UPGRADE,
  PACKET_UNIT_DO_ACTION,
  PACKET_UNIT_ACTION_ANSWER,
  PACKET_UNIT_GET_ACTIONS = 87,
  PACKET_CONN_PING,
  PACKET_CONN_PONG,
  PACKET_UNIT_ACTIONS,                   /* 90 */
  PACKET_DIPLOMACY_INIT_MEETING_REQ = 95,
  PACKET_DIPLOMACY_INIT_MEETING,
  PACKET_DIPLOMACY_CANCEL_MEETING_REQ,
  PACKET_DIPLOMACY_CANCEL_MEETING,
  PACKET_DIPLOMACY_CREATE_CLAUSE_REQ,
  PACKET_DIPLOMACY_CREATE_CLAUSE,        /* 100 */
  PACKET_DIPLOMACY_REMOVE_CLAUSE_REQ,
  PACKET_DIPLOMACY_REMOVE_CLAUSE,
  PACKET_DIPLOMACY_ACCEPT_TREATY_REQ,
  PACKET_DIPLOMACY_ACCEPT_TREATY,
  PACKET_DIPLOMACY_CANCEL_PACT,
  PACKET_PAGE_MSG = 110,                 /* 110 */
  PACKET_REPORT_REQ,
  PACKET_CONN_INFO = 115,
  PACKET_CONN_PING_INFO,
  PACKET_CLIENT_INFO = 119,
  PACKET_END_PHASE = 125,
  PACKET_START_PHASE,
  PACKET_NEW_YEAR,
  PACKET_BEGIN_TURN,
  PACKET_END_TURN,
  PACKET_FREEZE_CLIENT,                  /* 130 */
  PACKET_THAW_CLIENT,
  PACKET_SPACESHIP_LAUNCH = 135,
  PACKET_SPACESHIP_PLACE,
  PACKET_SPACESHIP_INFO,
  PACKET_CITY_RALLY_POINT,
  PACKET_RULESET_UNIT = 140,             /* 140 */
  PACKET_RULESET_GAME,
  PACKET_RULESET_SPECIALIST,
  PACKET_RULESET_GOVERNMENT_RULER_TITLE,
  PACKET_RULESET_TECH,
  PACKET_RULESET_GOVERNMENT,
  PACKET_RULESET_TERRAIN_CONTROL,
  PACKET_RULESET_NATION_GROUPS,
  PACKET_RULESET_NATION,
  PACKET_RULESET_CITY,
  PACKET_RULESET_BUILDING,               /* 150 */
  PACKET_RULESET_TERRAIN,
  PACKET_RULESET_UNIT_CLASS,
  PACKET_RULESET_BASE,
  PACKET_RULESET_CONTROL = 155,
  PACKET_SINGLE_WANT_HACK_REQ = 160,     /* 160 */
  PACKET_SINGLE_WANT_HACK_REPLY,
  PACKET_RULESET_CHOICES,
  PACKET_GAME_LOAD,
  PACKET_SERVER_SETTING_CONTROL,
  PACKET_SERVER_SETTING_CONST,
  PACKET_SERVER_SETTING_BOOL,
  PACKET_SERVER_SETTING_INT,
  PACKET_SERVER_SETTING_STR,
  PACKET_SERVER_SETTING_ENUM,
  PACKET_SERVER_SETTING_BITWISE,         /* 170 */
  PACKET_RULESET_EFFECT = 175,
  PACKET_RULESET_RESOURCE = 177,
  PACKET_SCENARIO_INFO = 180,            /* 180 */
  PACKET_SAVE_SCENARIO,
  PACKET_VOTE_NEW = 185,
  PACKET_VOTE_UPDATE,
  PACKET_VOTE_REMOVE,
  PACKET_VOTE_RESOLVE,
  PACKET_VOTE_SUBMIT,
  PACKET_EDIT_MODE,                      /* 190 */
  PACKET_EDIT_RECALCULATE_BORDERS = 197,
  PACKET_EDIT_CHECK_TILES,
  PACKET_EDIT_TOGGLE_FOGOFWAR,
  PACKET_EDIT_TILE_TERRAIN,              /* 200 */
  PACKET_EDIT_TILE_EXTRA = 202,
  PACKET_EDIT_STARTPOS = 204,
  PACKET_EDIT_STARTPOS_FULL,
  PACKET_EDIT_TILE,
  PACKET_EDIT_UNIT_CREATE,
  PACKET_EDIT_UNIT_REMOVE,
  PACKET_EDIT_UNIT_REMOVE_BY_ID,
  PACKET_EDIT_UNIT,                      /* 210 */
  PACKET_EDIT_CITY_CREATE,
  PACKET_EDIT_CITY_REMOVE,
  PACKET_EDIT_CITY,
  PACKET_EDIT_PLAYER_CREATE,
  PACKET_EDIT_PLAYER_REMOVE,
  PACKET_EDIT_PLAYER,
  PACKET_EDIT_PLAYER_VISION,
  PACKET_EDIT_GAME,
  PACKET_EDIT_OBJECT_CREATED,
  PACKET_RULESET_ROAD,                   /* 220 */
  PACKET_UNIT_CHANGE_ACTIVITY = 222,
  PACKET_ENDGAME_PLAYER,
  PACKET_RULESET_DISASTER,
  PACKET_RULESETS_READY,
  PACKET_RULESET_EXTRA_FLAG,
  PACKET_RULESET_TRADE,
  PACKET_RULESET_UNIT_BONUS,
  PACKET_RULESET_UNIT_FLAG,
  PACKET_RULESET_UNIT_CLASS_FLAG,        /* 230 */
  PACKET_RULESET_TERRAIN_FLAG,
  PACKET_RULESET_EXTRA,
  PACKET_RULESET_ACHIEVEMENT,
  PACKET_RULESET_TECH_FLAG,
  PACKET_RULESET_ACTION_ENABLER,
  PACKET_RULESET_NATION_SETS,
  PACKET_NATION_AVAILABILITY,
  PACKET_ACHIEVEMENT_INFO,
  PACKET_RULESET_STYLE,
  PACKET_RULESET_MUSIC,                  /* 240 */
  PACKET_WORKER_TASK,
  PACKET_PLAYER_MULTIPLIER,
  PACKET_RULESET_MULTIPLIER,
  PACKET_TIMEOUT_INFO,
  PACKET_PLAY_MUSIC,
  PACKET_RULESET_ACTION,
  PACKET_RULESET_DESCRIPTION_PART,
  PACKET_RULESET_GOODS,
  PACKET_TRADE_ROUTE_INFO,
  PACKET_PAGE_MSG_PART,                  /* 250 */
  PACKET_RULESET_SUMMARY,
  PACKET_RULESET_ACTION_AUTO,
  PACKET_SET_TOPOLOGY,
  PACKET_CLIENT_HEARTBEAT,
  PACKET_CALENDAR_INFO,
  PACKET_WEB_CITY_INFO_ADDITION,
  PACKET_WEB_PLAYER_INFO_ADDITION,
  PACKET_WEB_RULESET_UNIT_ADDITION,
  PACKET_RULESET_CLAUSE = 512,

  PACKET_LAST  /* leave this last */
};

int send_packet_processing_started(struct connection *pc);

int send_packet_processing_finished(struct connection *pc);

int send_packet_investigate_started(struct connection *pc, const struct packet_investigate_started *packet);
int dsend_packet_investigate_started(struct connection *pc, int unit_id16, int unit_id32, int city_id16, int city_id32);

int send_packet_investigate_finished(struct connection *pc, const struct packet_investigate_finished *packet);
int dsend_packet_investigate_finished(struct connection *pc, int unit_id16, int unit_id32, int city_id16, int city_id32);

int send_packet_server_join_req(struct connection *pc, const struct packet_server_join_req *packet);
int dsend_packet_server_join_req(struct connection *pc, const char *username, const char *capability, const char *version_label, int major_version, int minor_version, int patch_version);

int send_packet_server_join_reply(struct connection *pc, const struct packet_server_join_reply *packet);

int send_packet_authentication_req(struct connection *pc, const struct packet_authentication_req *packet);
int dsend_packet_authentication_req(struct connection *pc, enum authentication_type type, const char *message);

int send_packet_authentication_reply(struct connection *pc, const struct packet_authentication_reply *packet);

int send_packet_server_shutdown(struct connection *pc);
void lsend_packet_server_shutdown(struct conn_list *dest);

int send_packet_nation_select_req(struct connection *pc, const struct packet_nation_select_req *packet);
int dsend_packet_nation_select_req(struct connection *pc, int player_no, Nation_type_id nation_no, bool is_male, const char *name, int style);

int send_packet_player_ready(struct connection *pc, const struct packet_player_ready *packet);
int dsend_packet_player_ready(struct connection *pc, int player_no, bool is_ready);

int send_packet_endgame_report(struct connection *pc, const struct packet_endgame_report *packet);
void lsend_packet_endgame_report(struct conn_list *dest, const struct packet_endgame_report *packet);

int send_packet_endgame_player(struct connection *pc, const struct packet_endgame_player *packet);
void lsend_packet_endgame_player(struct conn_list *dest, const struct packet_endgame_player *packet);

int send_packet_tile_info(struct connection *pc, const struct packet_tile_info *packet);
void lsend_packet_tile_info(struct conn_list *dest, const struct packet_tile_info *packet);

int send_packet_game_info(struct connection *pc, const struct packet_game_info *packet);

int send_packet_calendar_info(struct connection *pc, const struct packet_calendar_info *packet);

int send_packet_timeout_info(struct connection *pc, const struct packet_timeout_info *packet);

int send_packet_map_info(struct connection *pc, const struct packet_map_info *packet);
void lsend_packet_map_info(struct conn_list *dest, const struct packet_map_info *packet);

int send_packet_nuke_tile_info(struct connection *pc, const struct packet_nuke_tile_info *packet);
void lsend_packet_nuke_tile_info(struct conn_list *dest, const struct packet_nuke_tile_info *packet);
int dsend_packet_nuke_tile_info(struct connection *pc, int tile);
void dlsend_packet_nuke_tile_info(struct conn_list *dest, int tile);

int send_packet_team_name_info(struct connection *pc, const struct packet_team_name_info *packet);
void lsend_packet_team_name_info(struct conn_list *dest, const struct packet_team_name_info *packet);

int send_packet_achievement_info(struct connection *pc, const struct packet_achievement_info *packet);
void lsend_packet_achievement_info(struct conn_list *dest, const struct packet_achievement_info *packet);

int send_packet_chat_msg(struct connection *pc, const struct packet_chat_msg *packet);
void lsend_packet_chat_msg(struct conn_list *dest, const struct packet_chat_msg *packet);

int send_packet_early_chat_msg(struct connection *pc, const struct packet_early_chat_msg *packet);
void lsend_packet_early_chat_msg(struct conn_list *dest, const struct packet_early_chat_msg *packet);

int send_packet_chat_msg_req(struct connection *pc, const struct packet_chat_msg_req *packet);
int dsend_packet_chat_msg_req(struct connection *pc, const char *message);

int send_packet_connect_msg(struct connection *pc, const struct packet_connect_msg *packet);
int dsend_packet_connect_msg(struct connection *pc, const char *message);

int send_packet_server_info(struct connection *pc, const struct packet_server_info *packet);
int dsend_packet_server_info(struct connection *pc, const char *version_label, int major_version, int minor_version, int patch_version, int emerg_version);

int send_packet_city_remove(struct connection *pc, const struct packet_city_remove *packet);
void lsend_packet_city_remove(struct conn_list *dest, const struct packet_city_remove *packet);
int dsend_packet_city_remove(struct connection *pc, int city_id16, int city_id32);
void dlsend_packet_city_remove(struct conn_list *dest, int city_id16, int city_id32);

int send_packet_city_info(struct connection *pc, const struct packet_city_info *packet, bool force_to_send);
void lsend_packet_city_info(struct conn_list *dest, const struct packet_city_info *packet, bool force_to_send);

int send_packet_city_nationalities(struct connection *pc, const struct packet_city_nationalities *packet, bool force_to_send);
void lsend_packet_city_nationalities(struct conn_list *dest, const struct packet_city_nationalities *packet, bool force_to_send);

int send_packet_city_short_info(struct connection *pc, const struct packet_city_short_info *packet);
void lsend_packet_city_short_info(struct conn_list *dest, const struct packet_city_short_info *packet);

int send_packet_trade_route_info(struct connection *pc, const struct packet_trade_route_info *packet);
void lsend_packet_trade_route_info(struct conn_list *dest, const struct packet_trade_route_info *packet);

int send_packet_city_sell(struct connection *pc, const struct packet_city_sell *packet);
int dsend_packet_city_sell(struct connection *pc, int city_id16, int city_id32, int build_id);

int send_packet_city_buy(struct connection *pc, const struct packet_city_buy *packet);
int dsend_packet_city_buy(struct connection *pc, int city_id16, int city_id32);

int send_packet_city_change(struct connection *pc, const struct packet_city_change *packet);
int dsend_packet_city_change(struct connection *pc, int city_id16, int city_id32, int production_kind, int production_value);

int send_packet_city_worklist(struct connection *pc, const struct packet_city_worklist *packet);
int dsend_packet_city_worklist(struct connection *pc, int city_id16, int city_id32, const struct worklist *worklist);

int send_packet_city_make_specialist(struct connection *pc, const struct packet_city_make_specialist *packet);
int dsend_packet_city_make_specialist(struct connection *pc, int city_id16, int city_id32, int tile_id);

int send_packet_city_make_worker(struct connection *pc, const struct packet_city_make_worker *packet);
int dsend_packet_city_make_worker(struct connection *pc, int city_id16, int city_id32, int tile_id);

int send_packet_city_change_specialist(struct connection *pc, const struct packet_city_change_specialist *packet);
int dsend_packet_city_change_specialist(struct connection *pc, int city_id16, int city_id32, Specialist_type_id from, Specialist_type_id to);

int send_packet_city_rename(struct connection *pc, const struct packet_city_rename *packet);
int dsend_packet_city_rename(struct connection *pc, int city_id16, int city_id32, const char *name);

int send_packet_city_options_req(struct connection *pc, const struct packet_city_options_req *packet);
int dsend_packet_city_options_req(struct connection *pc, int city_id16, int city_id32, bv_city_options options);

int send_packet_city_refresh(struct connection *pc, const struct packet_city_refresh *packet);
int dsend_packet_city_refresh(struct connection *pc, int city_id16, int city_id32);

int send_packet_city_name_suggestion_req(struct connection *pc, const struct packet_city_name_suggestion_req *packet);
int dsend_packet_city_name_suggestion_req(struct connection *pc, int unit_id16, int unit_id32);

int send_packet_city_name_suggestion_info(struct connection *pc, const struct packet_city_name_suggestion_info *packet);
void lsend_packet_city_name_suggestion_info(struct conn_list *dest, const struct packet_city_name_suggestion_info *packet);
int dsend_packet_city_name_suggestion_info(struct connection *pc, int unit_id16, int unit_id32, const char *name);
void dlsend_packet_city_name_suggestion_info(struct conn_list *dest, int unit_id16, int unit_id32, const char *name);

int send_packet_city_sabotage_list(struct connection *pc, const struct packet_city_sabotage_list *packet);
void lsend_packet_city_sabotage_list(struct conn_list *dest, const struct packet_city_sabotage_list *packet);

int send_packet_city_rally_point(struct connection *pc, const struct packet_city_rally_point *packet, bool force_to_send);
void lsend_packet_city_rally_point(struct conn_list *dest, const struct packet_city_rally_point *packet, bool force_to_send);

int send_packet_worker_task(struct connection *pc, const struct packet_worker_task *packet);
void lsend_packet_worker_task(struct conn_list *dest, const struct packet_worker_task *packet);

int send_packet_player_remove(struct connection *pc, const struct packet_player_remove *packet);
int dsend_packet_player_remove(struct connection *pc, int playerno);

int send_packet_player_info(struct connection *pc, const struct packet_player_info *packet);

int send_packet_player_phase_done(struct connection *pc, const struct packet_player_phase_done *packet);
int dsend_packet_player_phase_done(struct connection *pc, int turn);

int send_packet_player_rates(struct connection *pc, const struct packet_player_rates *packet);
int dsend_packet_player_rates(struct connection *pc, int tax, int luxury, int science);

int send_packet_player_change_government(struct connection *pc, const struct packet_player_change_government *packet);
int dsend_packet_player_change_government(struct connection *pc, Government_type_id government);

int send_packet_player_place_infra(struct connection *pc, const struct packet_player_place_infra *packet);
int dsend_packet_player_place_infra(struct connection *pc, int tile, int extra);

int send_packet_player_attribute_block(struct connection *pc);

int send_packet_player_attribute_chunk(struct connection *pc, const struct packet_player_attribute_chunk *packet);

int send_packet_player_diplstate(struct connection *pc, const struct packet_player_diplstate *packet);

int send_packet_player_multiplier(struct connection *pc, const struct packet_player_multiplier *packet);

int send_packet_research_info(struct connection *pc, const struct packet_research_info *packet);
void lsend_packet_research_info(struct conn_list *dest, const struct packet_research_info *packet);

int send_packet_unknown_research(struct connection *pc, const struct packet_unknown_research *packet);

int send_packet_player_research(struct connection *pc, const struct packet_player_research *packet);
int dsend_packet_player_research(struct connection *pc, int tech);

int send_packet_player_tech_goal(struct connection *pc, const struct packet_player_tech_goal *packet);
int dsend_packet_player_tech_goal(struct connection *pc, int tech);

int send_packet_unit_remove(struct connection *pc, const struct packet_unit_remove *packet);
void lsend_packet_unit_remove(struct conn_list *dest, const struct packet_unit_remove *packet);
int dsend_packet_unit_remove(struct connection *pc, int unit_id16, int unit_id32);
void dlsend_packet_unit_remove(struct conn_list *dest, int unit_id16, int unit_id32);

int send_packet_unit_info(struct connection *pc, const struct packet_unit_info *packet);
void lsend_packet_unit_info(struct conn_list *dest, const struct packet_unit_info *packet);

int send_packet_unit_short_info(struct connection *pc, const struct packet_unit_short_info *packet, bool force_to_send);
void lsend_packet_unit_short_info(struct conn_list *dest, const struct packet_unit_short_info *packet, bool force_to_send);

int send_packet_unit_combat_info(struct connection *pc, const struct packet_unit_combat_info *packet);
void lsend_packet_unit_combat_info(struct conn_list *dest, const struct packet_unit_combat_info *packet);

int send_packet_unit_sscs_set(struct connection *pc, const struct packet_unit_sscs_set *packet);
int dsend_packet_unit_sscs_set(struct connection *pc, int unit_id16, int unit_id32, enum unit_ss_data_type type, int value);

int send_packet_unit_orders(struct connection *pc, const struct packet_unit_orders *packet);

int send_packet_unit_server_side_agent_set(struct connection *pc, const struct packet_unit_server_side_agent_set *packet);
int dsend_packet_unit_server_side_agent_set(struct connection *pc, int unit_id16, int unit_id32, enum server_side_agent agent);

int send_packet_unit_action_query(struct connection *pc, const struct packet_unit_action_query *packet);
int dsend_packet_unit_action_query(struct connection *pc, int actor_id16, int actor_id32, int target_id, action_id action_type, int request_kind);

int send_packet_unit_type_upgrade(struct connection *pc, const struct packet_unit_type_upgrade *packet);
int dsend_packet_unit_type_upgrade(struct connection *pc, Unit_type_id type);

int send_packet_unit_do_action(struct connection *pc, const struct packet_unit_do_action *packet);
int dsend_packet_unit_do_action(struct connection *pc, int actor_id16, int actor_id32, int target_id, int sub_tgt_id, const char *name, action_id action_type);

int send_packet_unit_action_answer(struct connection *pc, const struct packet_unit_action_answer *packet);
int dsend_packet_unit_action_answer(struct connection *pc, int actor_id16, int actor_id32, int target_id, int cost, action_id action_type, int request_kind);

int send_packet_unit_get_actions(struct connection *pc, const struct packet_unit_get_actions *packet);
int dsend_packet_unit_get_actions(struct connection *pc, int actor_unit_id16, int actor_unit_id32, int target_unit_id16, int target_unit_id32, int target_tile_id, int target_extra_id, int request_kind);

int send_packet_unit_actions(struct connection *pc, const struct packet_unit_actions *packet);
int dsend_packet_unit_actions(struct connection *pc, int actor_unit_id16, int actor_unit_id32, int target_unit_id16, int target_unit_id32, int target_city_id16, int target_city_id32, int target_tile_id, int target_extra_id, int request_kind, const struct act_prob *action_probabilities);

int send_packet_unit_change_activity(struct connection *pc, const struct packet_unit_change_activity *packet);
int dsend_packet_unit_change_activity(struct connection *pc, int unit_id16, int unit_id32, enum unit_activity activity, int target);

int send_packet_diplomacy_init_meeting_req(struct connection *pc, const struct packet_diplomacy_init_meeting_req *packet);
int dsend_packet_diplomacy_init_meeting_req(struct connection *pc, int counterpart);

int send_packet_diplomacy_init_meeting(struct connection *pc, const struct packet_diplomacy_init_meeting *packet);
void lsend_packet_diplomacy_init_meeting(struct conn_list *dest, const struct packet_diplomacy_init_meeting *packet);
int dsend_packet_diplomacy_init_meeting(struct connection *pc, int counterpart, int initiated_from);
void dlsend_packet_diplomacy_init_meeting(struct conn_list *dest, int counterpart, int initiated_from);

int send_packet_diplomacy_cancel_meeting_req(struct connection *pc, const struct packet_diplomacy_cancel_meeting_req *packet);
int dsend_packet_diplomacy_cancel_meeting_req(struct connection *pc, int counterpart);

int send_packet_diplomacy_cancel_meeting(struct connection *pc, const struct packet_diplomacy_cancel_meeting *packet);
void lsend_packet_diplomacy_cancel_meeting(struct conn_list *dest, const struct packet_diplomacy_cancel_meeting *packet);
int dsend_packet_diplomacy_cancel_meeting(struct connection *pc, int counterpart, int initiated_from);
void dlsend_packet_diplomacy_cancel_meeting(struct conn_list *dest, int counterpart, int initiated_from);

int send_packet_diplomacy_create_clause_req(struct connection *pc, const struct packet_diplomacy_create_clause_req *packet);
int dsend_packet_diplomacy_create_clause_req(struct connection *pc, int counterpart, int giver, enum clause_type type, int value);

int send_packet_diplomacy_create_clause(struct connection *pc, const struct packet_diplomacy_create_clause *packet);
void lsend_packet_diplomacy_create_clause(struct conn_list *dest, const struct packet_diplomacy_create_clause *packet);
int dsend_packet_diplomacy_create_clause(struct connection *pc, int counterpart, int giver, enum clause_type type, int value);
void dlsend_packet_diplomacy_create_clause(struct conn_list *dest, int counterpart, int giver, enum clause_type type, int value);

int send_packet_diplomacy_remove_clause_req(struct connection *pc, const struct packet_diplomacy_remove_clause_req *packet);
int dsend_packet_diplomacy_remove_clause_req(struct connection *pc, int counterpart, int giver, enum clause_type type, int value);

int send_packet_diplomacy_remove_clause(struct connection *pc, const struct packet_diplomacy_remove_clause *packet);
void lsend_packet_diplomacy_remove_clause(struct conn_list *dest, const struct packet_diplomacy_remove_clause *packet);
int dsend_packet_diplomacy_remove_clause(struct connection *pc, int counterpart, int giver, enum clause_type type, int value);
void dlsend_packet_diplomacy_remove_clause(struct conn_list *dest, int counterpart, int giver, enum clause_type type, int value);

int send_packet_diplomacy_accept_treaty_req(struct connection *pc, const struct packet_diplomacy_accept_treaty_req *packet);
int dsend_packet_diplomacy_accept_treaty_req(struct connection *pc, int counterpart);

int send_packet_diplomacy_accept_treaty(struct connection *pc, const struct packet_diplomacy_accept_treaty *packet);
void lsend_packet_diplomacy_accept_treaty(struct conn_list *dest, const struct packet_diplomacy_accept_treaty *packet);
int dsend_packet_diplomacy_accept_treaty(struct connection *pc, int counterpart, bool I_accepted, bool other_accepted);
void dlsend_packet_diplomacy_accept_treaty(struct conn_list *dest, int counterpart, bool I_accepted, bool other_accepted);

int send_packet_diplomacy_cancel_pact(struct connection *pc, const struct packet_diplomacy_cancel_pact *packet);
int dsend_packet_diplomacy_cancel_pact(struct connection *pc, int other_player_id, enum clause_type clause);

int send_packet_page_msg(struct connection *pc, const struct packet_page_msg *packet);
void lsend_packet_page_msg(struct conn_list *dest, const struct packet_page_msg *packet);

int send_packet_page_msg_part(struct connection *pc, const struct packet_page_msg_part *packet);
void lsend_packet_page_msg_part(struct conn_list *dest, const struct packet_page_msg_part *packet);

int send_packet_report_req(struct connection *pc, const struct packet_report_req *packet);
int dsend_packet_report_req(struct connection *pc, enum report_type type);

int send_packet_conn_info(struct connection *pc, const struct packet_conn_info *packet);
void lsend_packet_conn_info(struct conn_list *dest, const struct packet_conn_info *packet);

int send_packet_conn_ping_info(struct connection *pc, const struct packet_conn_ping_info *packet);
void lsend_packet_conn_ping_info(struct conn_list *dest, const struct packet_conn_ping_info *packet);

int send_packet_conn_ping(struct connection *pc);

int send_packet_conn_pong(struct connection *pc);

int send_packet_client_heartbeat(struct connection *pc);

int send_packet_client_info(struct connection *pc, const struct packet_client_info *packet);

int send_packet_end_phase(struct connection *pc);
void lsend_packet_end_phase(struct conn_list *dest);

int send_packet_start_phase(struct connection *pc, const struct packet_start_phase *packet);
void lsend_packet_start_phase(struct conn_list *dest, const struct packet_start_phase *packet);
int dsend_packet_start_phase(struct connection *pc, int phase);
void dlsend_packet_start_phase(struct conn_list *dest, int phase);

int send_packet_new_year(struct connection *pc, const struct packet_new_year *packet);
void lsend_packet_new_year(struct conn_list *dest, const struct packet_new_year *packet);

int send_packet_begin_turn(struct connection *pc);
void lsend_packet_begin_turn(struct conn_list *dest);

int send_packet_end_turn(struct connection *pc);
void lsend_packet_end_turn(struct conn_list *dest);

int send_packet_freeze_client(struct connection *pc);
void lsend_packet_freeze_client(struct conn_list *dest);

int send_packet_thaw_client(struct connection *pc);
void lsend_packet_thaw_client(struct conn_list *dest);

int send_packet_spaceship_launch(struct connection *pc);

int send_packet_spaceship_place(struct connection *pc, const struct packet_spaceship_place *packet);
int dsend_packet_spaceship_place(struct connection *pc, enum spaceship_place_type type, int num);

int send_packet_spaceship_info(struct connection *pc, const struct packet_spaceship_info *packet);
void lsend_packet_spaceship_info(struct conn_list *dest, const struct packet_spaceship_info *packet);

int send_packet_ruleset_unit(struct connection *pc, const struct packet_ruleset_unit *packet);
void lsend_packet_ruleset_unit(struct conn_list *dest, const struct packet_ruleset_unit *packet);

int send_packet_ruleset_unit_bonus(struct connection *pc, const struct packet_ruleset_unit_bonus *packet);
void lsend_packet_ruleset_unit_bonus(struct conn_list *dest, const struct packet_ruleset_unit_bonus *packet);

int send_packet_ruleset_unit_flag(struct connection *pc, const struct packet_ruleset_unit_flag *packet);
void lsend_packet_ruleset_unit_flag(struct conn_list *dest, const struct packet_ruleset_unit_flag *packet);

int send_packet_ruleset_unit_class_flag(struct connection *pc, const struct packet_ruleset_unit_class_flag *packet);
void lsend_packet_ruleset_unit_class_flag(struct conn_list *dest, const struct packet_ruleset_unit_class_flag *packet);

int send_packet_ruleset_game(struct connection *pc, const struct packet_ruleset_game *packet);
void lsend_packet_ruleset_game(struct conn_list *dest, const struct packet_ruleset_game *packet);

int send_packet_ruleset_specialist(struct connection *pc, const struct packet_ruleset_specialist *packet);
void lsend_packet_ruleset_specialist(struct conn_list *dest, const struct packet_ruleset_specialist *packet);

int send_packet_ruleset_government_ruler_title(struct connection *pc, const struct packet_ruleset_government_ruler_title *packet);
void lsend_packet_ruleset_government_ruler_title(struct conn_list *dest, const struct packet_ruleset_government_ruler_title *packet);

int send_packet_ruleset_tech(struct connection *pc, const struct packet_ruleset_tech *packet);
void lsend_packet_ruleset_tech(struct conn_list *dest, const struct packet_ruleset_tech *packet);

int send_packet_ruleset_tech_class(struct connection *pc, const struct packet_ruleset_tech_class *packet);
void lsend_packet_ruleset_tech_class(struct conn_list *dest, const struct packet_ruleset_tech_class *packet);

int send_packet_ruleset_tech_flag(struct connection *pc, const struct packet_ruleset_tech_flag *packet);
void lsend_packet_ruleset_tech_flag(struct conn_list *dest, const struct packet_ruleset_tech_flag *packet);

int send_packet_ruleset_government(struct connection *pc, const struct packet_ruleset_government *packet);
void lsend_packet_ruleset_government(struct conn_list *dest, const struct packet_ruleset_government *packet);

int send_packet_ruleset_terrain_control(struct connection *pc, const struct packet_ruleset_terrain_control *packet);
void lsend_packet_ruleset_terrain_control(struct conn_list *dest, const struct packet_ruleset_terrain_control *packet);

int send_packet_rulesets_ready(struct connection *pc);
void lsend_packet_rulesets_ready(struct conn_list *dest);

int send_packet_ruleset_nation_sets(struct connection *pc, const struct packet_ruleset_nation_sets *packet);
void lsend_packet_ruleset_nation_sets(struct conn_list *dest, const struct packet_ruleset_nation_sets *packet);

int send_packet_ruleset_nation_groups(struct connection *pc, const struct packet_ruleset_nation_groups *packet);
void lsend_packet_ruleset_nation_groups(struct conn_list *dest, const struct packet_ruleset_nation_groups *packet);

int send_packet_ruleset_nation(struct connection *pc, const struct packet_ruleset_nation *packet);
void lsend_packet_ruleset_nation(struct conn_list *dest, const struct packet_ruleset_nation *packet);

int send_packet_nation_availability(struct connection *pc, const struct packet_nation_availability *packet);
void lsend_packet_nation_availability(struct conn_list *dest, const struct packet_nation_availability *packet);

int send_packet_ruleset_style(struct connection *pc, const struct packet_ruleset_style *packet);
void lsend_packet_ruleset_style(struct conn_list *dest, const struct packet_ruleset_style *packet);

int send_packet_ruleset_city(struct connection *pc, const struct packet_ruleset_city *packet);
void lsend_packet_ruleset_city(struct conn_list *dest, const struct packet_ruleset_city *packet);

int send_packet_ruleset_building(struct connection *pc, const struct packet_ruleset_building *packet);
void lsend_packet_ruleset_building(struct conn_list *dest, const struct packet_ruleset_building *packet);

int send_packet_ruleset_terrain(struct connection *pc, const struct packet_ruleset_terrain *packet);
void lsend_packet_ruleset_terrain(struct conn_list *dest, const struct packet_ruleset_terrain *packet);

int send_packet_ruleset_terrain_flag(struct connection *pc, const struct packet_ruleset_terrain_flag *packet);
void lsend_packet_ruleset_terrain_flag(struct conn_list *dest, const struct packet_ruleset_terrain_flag *packet);

int send_packet_ruleset_unit_class(struct connection *pc, const struct packet_ruleset_unit_class *packet);
void lsend_packet_ruleset_unit_class(struct conn_list *dest, const struct packet_ruleset_unit_class *packet);

int send_packet_ruleset_extra(struct connection *pc, const struct packet_ruleset_extra *packet);
void lsend_packet_ruleset_extra(struct conn_list *dest, const struct packet_ruleset_extra *packet);

int send_packet_ruleset_extra_flag(struct connection *pc, const struct packet_ruleset_extra_flag *packet);
void lsend_packet_ruleset_extra_flag(struct conn_list *dest, const struct packet_ruleset_extra_flag *packet);

int send_packet_ruleset_base(struct connection *pc, const struct packet_ruleset_base *packet);
void lsend_packet_ruleset_base(struct conn_list *dest, const struct packet_ruleset_base *packet);

int send_packet_ruleset_road(struct connection *pc, const struct packet_ruleset_road *packet);
void lsend_packet_ruleset_road(struct conn_list *dest, const struct packet_ruleset_road *packet);

int send_packet_ruleset_goods(struct connection *pc, const struct packet_ruleset_goods *packet);
void lsend_packet_ruleset_goods(struct conn_list *dest, const struct packet_ruleset_goods *packet);

int send_packet_ruleset_disaster(struct connection *pc, const struct packet_ruleset_disaster *packet);
void lsend_packet_ruleset_disaster(struct conn_list *dest, const struct packet_ruleset_disaster *packet);

int send_packet_ruleset_achievement(struct connection *pc, const struct packet_ruleset_achievement *packet);
void lsend_packet_ruleset_achievement(struct conn_list *dest, const struct packet_ruleset_achievement *packet);

int send_packet_ruleset_trade(struct connection *pc, const struct packet_ruleset_trade *packet);
void lsend_packet_ruleset_trade(struct conn_list *dest, const struct packet_ruleset_trade *packet);

int send_packet_ruleset_action(struct connection *pc, const struct packet_ruleset_action *packet);
void lsend_packet_ruleset_action(struct conn_list *dest, const struct packet_ruleset_action *packet);

int send_packet_ruleset_action_enabler(struct connection *pc, const struct packet_ruleset_action_enabler *packet);
void lsend_packet_ruleset_action_enabler(struct conn_list *dest, const struct packet_ruleset_action_enabler *packet);

int send_packet_ruleset_action_auto(struct connection *pc, const struct packet_ruleset_action_auto *packet);
void lsend_packet_ruleset_action_auto(struct conn_list *dest, const struct packet_ruleset_action_auto *packet);

int send_packet_ruleset_music(struct connection *pc, const struct packet_ruleset_music *packet);
void lsend_packet_ruleset_music(struct conn_list *dest, const struct packet_ruleset_music *packet);

int send_packet_ruleset_multiplier(struct connection *pc, const struct packet_ruleset_multiplier *packet);
void lsend_packet_ruleset_multiplier(struct conn_list *dest, const struct packet_ruleset_multiplier *packet);
int dsend_packet_ruleset_multiplier(struct connection *pc, Multiplier_type_id id, int start, int stop, int step, int def, int offset, int factor, int minimum_turns, const char *name, const char *rule_name, int reqs_count, const struct requirement *reqs, const char *helptext);
void dlsend_packet_ruleset_multiplier(struct conn_list *dest, Multiplier_type_id id, int start, int stop, int step, int def, int offset, int factor, int minimum_turns, const char *name, const char *rule_name, int reqs_count, const struct requirement *reqs, const char *helptext);

int send_packet_ruleset_clause(struct connection *pc, const struct packet_ruleset_clause *packet);
void lsend_packet_ruleset_clause(struct conn_list *dest, const struct packet_ruleset_clause *packet);

int send_packet_ruleset_control(struct connection *pc, const struct packet_ruleset_control *packet);
void lsend_packet_ruleset_control(struct conn_list *dest, const struct packet_ruleset_control *packet);

int send_packet_ruleset_summary(struct connection *pc, const struct packet_ruleset_summary *packet);
void lsend_packet_ruleset_summary(struct conn_list *dest, const struct packet_ruleset_summary *packet);

int send_packet_ruleset_description_part(struct connection *pc, const struct packet_ruleset_description_part *packet);
void lsend_packet_ruleset_description_part(struct conn_list *dest, const struct packet_ruleset_description_part *packet);

int send_packet_single_want_hack_req(struct connection *pc, const struct packet_single_want_hack_req *packet);

int send_packet_single_want_hack_reply(struct connection *pc, const struct packet_single_want_hack_reply *packet);
int dsend_packet_single_want_hack_reply(struct connection *pc, bool you_have_hack);

int send_packet_ruleset_choices(struct connection *pc, const struct packet_ruleset_choices *packet);

int send_packet_game_load(struct connection *pc, const struct packet_game_load *packet);
void lsend_packet_game_load(struct conn_list *dest, const struct packet_game_load *packet);
int dsend_packet_game_load(struct connection *pc, bool load_successful, const char *load_filename);
void dlsend_packet_game_load(struct conn_list *dest, bool load_successful, const char *load_filename);

int send_packet_server_setting_control(struct connection *pc, const struct packet_server_setting_control *packet);

int send_packet_server_setting_const(struct connection *pc, const struct packet_server_setting_const *packet);

int send_packet_server_setting_bool(struct connection *pc, const struct packet_server_setting_bool *packet);

int send_packet_server_setting_int(struct connection *pc, const struct packet_server_setting_int *packet);

int send_packet_server_setting_str(struct connection *pc, const struct packet_server_setting_str *packet);

int send_packet_server_setting_enum(struct connection *pc, const struct packet_server_setting_enum *packet);

int send_packet_server_setting_bitwise(struct connection *pc, const struct packet_server_setting_bitwise *packet);

int send_packet_set_topology(struct connection *pc, const struct packet_set_topology *packet);

int send_packet_ruleset_effect(struct connection *pc, const struct packet_ruleset_effect *packet);
void lsend_packet_ruleset_effect(struct conn_list *dest, const struct packet_ruleset_effect *packet);

int send_packet_ruleset_resource(struct connection *pc, const struct packet_ruleset_resource *packet);
void lsend_packet_ruleset_resource(struct conn_list *dest, const struct packet_ruleset_resource *packet);

int send_packet_scenario_info(struct connection *pc, const struct packet_scenario_info *packet);

int send_packet_scenario_description(struct connection *pc, const struct packet_scenario_description *packet);

int send_packet_save_scenario(struct connection *pc, const struct packet_save_scenario *packet);
int dsend_packet_save_scenario(struct connection *pc, const char *name);

int send_packet_vote_new(struct connection *pc, const struct packet_vote_new *packet);

int send_packet_vote_update(struct connection *pc, const struct packet_vote_update *packet);

int send_packet_vote_remove(struct connection *pc, const struct packet_vote_remove *packet);

int send_packet_vote_resolve(struct connection *pc, const struct packet_vote_resolve *packet);

int send_packet_vote_submit(struct connection *pc, const struct packet_vote_submit *packet);

int send_packet_edit_mode(struct connection *pc, const struct packet_edit_mode *packet);
int dsend_packet_edit_mode(struct connection *pc, bool state);

int send_packet_edit_recalculate_borders(struct connection *pc);

int send_packet_edit_check_tiles(struct connection *pc);

int send_packet_edit_toggle_fogofwar(struct connection *pc, const struct packet_edit_toggle_fogofwar *packet);
int dsend_packet_edit_toggle_fogofwar(struct connection *pc, int player);

int send_packet_edit_tile_terrain(struct connection *pc, const struct packet_edit_tile_terrain *packet);
int dsend_packet_edit_tile_terrain(struct connection *pc, int tile, Terrain_type_id terrain, int size);

int send_packet_edit_tile_extra(struct connection *pc, const struct packet_edit_tile_extra *packet);
int dsend_packet_edit_tile_extra(struct connection *pc, int tile, int extra_type_id, bool removal, int eowner, int size);

int send_packet_edit_startpos(struct connection *pc, const struct packet_edit_startpos *packet);
int dsend_packet_edit_startpos(struct connection *pc, int id, bool removal, int tag);

int send_packet_edit_startpos_full(struct connection *pc, const struct packet_edit_startpos_full *packet);

int send_packet_edit_tile(struct connection *pc, const struct packet_edit_tile *packet);

int send_packet_edit_unit_create(struct connection *pc, const struct packet_edit_unit_create *packet);
int dsend_packet_edit_unit_create(struct connection *pc, int owner, int tile, Unit_type_id type, int count, int tag);

int send_packet_edit_unit_remove(struct connection *pc, const struct packet_edit_unit_remove *packet);
int dsend_packet_edit_unit_remove(struct connection *pc, int owner, int tile, Unit_type_id type, int count);

int send_packet_edit_unit_remove_by_id(struct connection *pc, const struct packet_edit_unit_remove_by_id *packet);
int dsend_packet_edit_unit_remove_by_id(struct connection *pc, int id16, int id32);

int send_packet_edit_unit(struct connection *pc, const struct packet_edit_unit *packet);

int send_packet_edit_city_create(struct connection *pc, const struct packet_edit_city_create *packet);
int dsend_packet_edit_city_create(struct connection *pc, int owner, int tile, int size, int tag);

int send_packet_edit_city_remove(struct connection *pc, const struct packet_edit_city_remove *packet);
int dsend_packet_edit_city_remove(struct connection *pc, int id16, int id32);

int send_packet_edit_city(struct connection *pc, const struct packet_edit_city *packet);

int send_packet_edit_player_create(struct connection *pc, const struct packet_edit_player_create *packet);
int dsend_packet_edit_player_create(struct connection *pc, int tag);

int send_packet_edit_player_remove(struct connection *pc, const struct packet_edit_player_remove *packet);
int dsend_packet_edit_player_remove(struct connection *pc, int id);

int send_packet_edit_player(struct connection *pc, const struct packet_edit_player *packet);
void lsend_packet_edit_player(struct conn_list *dest, const struct packet_edit_player *packet);

int send_packet_edit_player_vision(struct connection *pc, const struct packet_edit_player_vision *packet);
int dsend_packet_edit_player_vision(struct connection *pc, int player, int tile, bool known, int size);

int send_packet_edit_game(struct connection *pc, const struct packet_edit_game *packet);

int send_packet_edit_scenario_desc(struct connection *pc, const struct packet_edit_scenario_desc *packet);

int send_packet_edit_object_created(struct connection *pc, const struct packet_edit_object_created *packet);
int dsend_packet_edit_object_created(struct connection *pc, int tag, int id);

int send_packet_play_music(struct connection *pc, const struct packet_play_music *packet);
void lsend_packet_play_music(struct conn_list *dest, const struct packet_play_music *packet);

int send_packet_web_city_info_addition(struct connection *pc, const struct packet_web_city_info_addition *packet, bool force_to_send);
void lsend_packet_web_city_info_addition(struct conn_list *dest, const struct packet_web_city_info_addition *packet, bool force_to_send);

int send_packet_web_player_info_addition(struct connection *pc, const struct packet_web_player_info_addition *packet);

int send_packet_web_ruleset_unit_addition(struct connection *pc, const struct packet_web_ruleset_unit_addition *packet);
void lsend_packet_web_ruleset_unit_addition(struct conn_list *dest, const struct packet_web_ruleset_unit_addition *packet);


void delta_stats_report(void);
void delta_stats_reset(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
