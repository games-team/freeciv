Source: freeciv
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Jordi Mallach <jordi@debian.org>,
 Clint Adams <clint@debian.org>,
 Markus Koschany <apo@debian.org>,
 Tobias Frost <tobi@debian.org>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.freeciv.org/
Vcs-Git: https://salsa.debian.org/games-team/freeciv.git
Vcs-Browser: https://salsa.debian.org/games-team/freeciv
Build-Depends:
 debhelper-compat (= 13),
 gettext,
 libbz2-dev,
 libcurl4-gnutls-dev,
 libgtk-3-dev,
 libgtk-4-dev,
 liblua5.4-dev,
 liblzma-dev,
 libreadline-dev,
 libsdl2-dev,
 libsdl2-image-dev,
 libsdl2-ttf-dev,
 libsdl2-mixer-dev,
 libsdl2-gfx-dev,
 libsqlite3-dev,
 libzstd-dev,
 qt6-base-dev,
 qt6-base-dev-tools,
 zlib1g-dev

Package: freeciv
Architecture: all
Multi-Arch: foreign
Depends:
 freeciv-client-gtk3,
 freeciv-data (= ${source:Version}),
 ${misc:Depends}
Description: Civilization turn based strategy game
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This metapackage will install the recommended client to play Freeciv.

Package: freeciv-client-extras
Architecture: any
Depends:
 freeciv-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: Civilization turn based strategy game (miscellaneous extras)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This package includes Freeciv's modpack tool freeciv-mp-gtk3. It allows you to
 select and download custom rulesets and tilesets for the game.

Package: freeciv-client-gtk3
Provides: freeciv-client (= ${binary:Version})
Architecture: any
Depends:
 freeciv-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 freeciv-server (= ${binary:Version})
Suggests:
 freeciv-client-extras,
Description: Civilization turn based strategy game (GTK 3 client)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This is the GTK 3 version of Freeciv. It is the most sophisticated and
 recommended client to play the game.

Package: freeciv-client-gtk4
Provides: freeciv-client (= ${binary:Version})
Architecture: any
Depends:
 freeciv-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 freeciv-server (= ${binary:Version})
Suggests:
 freeciv-client-extras,
Description: Civilization turn based strategy game (GTK 4 client)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This is the GTK 4 version of Freeciv.

Package: freeciv-client-qt
Provides: freeciv-client (= ${binary:Version})
Architecture: any
Depends:
 freeciv-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 freeciv-server (= ${binary:Version})
Suggests:
 freeciv-client-extras,
Description: Civilization turn based strategy game (Qt client)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This is the Qt version of Freeciv.

Package: freeciv-client-sdl
Provides: freeciv-client (= ${binary:Version})
Architecture: any
Depends:
 fonts-arphic-uming,
 fonts-dejavu-core,
 fonts-ipafont-gothic,
 fonts-noto-cjk,
 freeciv-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 freeciv-server (= ${binary:Version})
Suggests:
 freeciv-client-extras,
Description: Civilization turn based strategy game (SDL client)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This version of Freeciv is based on SDL, the Simple DirectMedia Layer library.
 It is recommended to install the »freeciv-client-gtk3« package and to use the
 GTK 3 client unless you really want to try this one. The SDL client offers a
 different look&feel but lacks some functionality of the GTK 3 client.

Package: freeciv-data
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Breaks: freeciv-sound-standard (<< 2.6.6-2)
Replaces: freeciv-sound-standard (<< 2.6.6-2)
Description: Civilization turn based strategy game (data)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 These are the common data and sound files for Freeciv.

Package: freeciv-server
Architecture: any
Depends:
 freeciv-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 freeciv
Description: Civilization turn based strategy game (server files)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 This is the Freeciv game server.

Package: freeciv-ruleset-tools
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 freeciv-server
Suggests:
 freeciv
Description: Civilization turn based strategy game (ruleset tools)
 Freeciv is a free clone of the turn based strategy game Civilization.
 In this game, each player becomes leader of a civilisation, fighting to
 obtain the ultimate goal: the extinction of all other civilisations.
 .
 The package contains tools to update and edit rulesets.
