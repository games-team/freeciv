This is tolua-5.2.4 modified for Freeciv usage.
Not entire tolua distribution directory hierarchy is included here, and
some files needed for Freeciv usage have been added.

Changes applied to included tolua source files are included in
patches in patch -directory.

Changes to toluabind.c are not included in the patches - one needs
to regenerate that file after applying them.
