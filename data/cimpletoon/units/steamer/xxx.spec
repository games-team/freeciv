
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    XYZ
"

[extra]
sprites =
 { "file",                          "tag"
   "cimpletoon/units/steamer/0001", "u.steamer_se", "u.raft_se"
   "cimpletoon/units/steamer/0002", "u.steamer_s", "u.raft_s"
   "cimpletoon/units/steamer/0003", "u.steamer_sw", "u.raft_sw"
   "cimpletoon/units/steamer/0004", "u.steamer_w", "u.raft_w"
   "cimpletoon/units/steamer/0005", "u.steamer_nw", "u.raft_nw"
   "cimpletoon/units/steamer/0006", "u.steamer_n", "u.raft_n"
   "cimpletoon/units/steamer/0007", "u.steamer_ne", "u.raft_ne"
   "cimpletoon/units/steamer/0008", "u.steamer_e", "u.raft_e"
 }
