
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    XYZ
"

[extra]
sprites =
 { "file",                          "tag"
   "cimpletoon/units/pt-boat/0001", "u.pt-boat_se", "u.amphi_transport_se"
   "cimpletoon/units/pt-boat/0002", "u.pt-boat_s", "u.amphi_transport_s"
   "cimpletoon/units/pt-boat/0003", "u.pt-boat_sw", "u.amphi_transport_sw"
   "cimpletoon/units/pt-boat/0004", "u.pt-boat_w", "u.amphi_transport_w"
   "cimpletoon/units/pt-boat/0005", "u.pt-boat_nw", "u.amphi_transport_nw"
   "cimpletoon/units/pt-boat/0006", "u.pt-boat_n", "u.amphi_transport_n"
   "cimpletoon/units/pt-boat/0007", "u.pt-boat_ne", "u.amphi_transport_ne"
   "cimpletoon/units/pt-boat/0008", "u.pt-boat_e", "u.amphi_transport_e"
 }
