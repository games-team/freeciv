
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    XYZ
"

[extra]
sprites =
 { "file",                            "tag"
   "cimpletoon/units/technical/0001", "u.technical_se", "u.native_engineer_se"
   "cimpletoon/units/technical/0002", "u.technical_s", "u.native_engineer_s"
   "cimpletoon/units/technical/0003", "u.technical_sw", "u.native_engineer_sw"
   "cimpletoon/units/technical/0004", "u.technical_w", "u.native_engineer_w"
   "cimpletoon/units/technical/0005", "u.technical_nw", "u.native_engineer_nw"
   "cimpletoon/units/technical/0006", "u.technical_n", "u.native_engineer_n"
   "cimpletoon/units/technical/0007", "u.technical_ne", "u.native_engineer_ne"
   "cimpletoon/units/technical/0008", "u.technical_e", "u.native_engineer_e"
 }
