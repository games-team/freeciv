
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    XYZ
"

[extra]
sprites =
 { "file",                             "tag"
   "cimpletoon/units/icebreaker/0001", "u.icebreaker_se", "u.water_engineer_se"
   "cimpletoon/units/icebreaker/0002", "u.icebreaker_s", "u.water_engineer_s"
   "cimpletoon/units/icebreaker/0003", "u.icebreaker_sw", "u.water_engineer_sw"
   "cimpletoon/units/icebreaker/0004", "u.icebreaker_w", "u.water_engineer_w"
   "cimpletoon/units/icebreaker/0005", "u.icebreaker_nw", "u.water_engineer_nw"
   "cimpletoon/units/icebreaker/0006", "u.icebreaker_n", "u.water_engineer_n"
   "cimpletoon/units/icebreaker/0007", "u.icebreaker_ne", "u.water_engineer_ne"
   "cimpletoon/units/icebreaker/0008", "u.icebreaker_e", "u.water_engineer_e"
 }
