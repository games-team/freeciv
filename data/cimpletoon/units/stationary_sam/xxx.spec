
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    Elefant
"

[extra]
sprites =
 { "file",                                 "tag"
   "cimpletoon/units/stationary_sam/0001", "u.stat_sam_se", "u.heavy_spitter_se"
   "cimpletoon/units/stationary_sam/0002", "u.stat_sam_s", "u.heavy_spitter_s"
   "cimpletoon/units/stationary_sam/0003", "u.stat_sam_sw", "u.heavy_spitter_sw"
   "cimpletoon/units/stationary_sam/0004", "u.stat_sam_w", "u.heavy_spitter_w"
   "cimpletoon/units/stationary_sam/0005", "u.stat_sam_nw", "u.heavy_spitter_nw"
   "cimpletoon/units/stationary_sam/0006", "u.stat_sam_n", "u.heavy_spitter_n"
   "cimpletoon/units/stationary_sam/0007", "u.stat_sam_ne", "u.heavy_spitter_ne"
   "cimpletoon/units/stationary_sam/0008", "u.stat_sam_e", "u.heavy_spitter_e"
 }
