
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    Elefant
"

[extra]
sprites =
 { "file",                            "tag"
   "cimpletoon/units/m4sherman/0001", "u.sherman_se", "u.turtle_defender_se"
   "cimpletoon/units/m4sherman/0002", "u.sherman_s", "u.turtle_defender_s"
   "cimpletoon/units/m4sherman/0003", "u.sherman_sw", "u.turtle_defender_sw"
   "cimpletoon/units/m4sherman/0004", "u.sherman_w", "u.turtle_defender_w"
   "cimpletoon/units/m4sherman/0005", "u.sherman_nw", "u.turtle_defender_nw"
   "cimpletoon/units/m4sherman/0006", "u.sherman_n", "u.turtle_defender_n"
   "cimpletoon/units/m4sherman/0007", "u.sherman_ne", "u.turtle_defender_ne"
   "cimpletoon/units/m4sherman/0008", "u.sherman_e", "u.turtle_defender_e"
 }
