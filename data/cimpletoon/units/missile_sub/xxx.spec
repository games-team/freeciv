
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    XYZ
"

[extra]
sprites =
 { "file",                        "tag"
   "cimpletoon/units/missile_sub/0001", "u.missile_sub_se", "u.hunter_se"
   "cimpletoon/units/missile_sub/0002", "u.missile_sub_s", "u.hunter_s"
   "cimpletoon/units/missile_sub/0003", "u.missile_sub_sw", "u.hunter_sw"
   "cimpletoon/units/missile_sub/0004", "u.missile_sub_w", "u.hunter_w"
   "cimpletoon/units/missile_sub/0005", "u.missile_sub_nw", "u.hunter_nw"
   "cimpletoon/units/missile_sub/0006", "u.missile_sub_n", "u.hunter_n"
   "cimpletoon/units/missile_sub/0007", "u.missile_sub_ne", "u.hunter_ne"
   "cimpletoon/units/missile_sub/0008", "u.missile_sub_e", "u.hunter_e"
 }
