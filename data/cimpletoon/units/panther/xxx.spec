
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    Elefant
"

[extra]
sprites =
 { "file",                          "tag"
   "cimpletoon/units/panther/0001", "u.panther_se", "u.tank_se"
   "cimpletoon/units/panther/0002", "u.panther_s", "u.tank_s"
   "cimpletoon/units/panther/0003", "u.panther_sw", "u.tank_sw"
   "cimpletoon/units/panther/0004", "u.panther_w", "u.tank_w"
   "cimpletoon/units/panther/0005", "u.panther_nw", "u.tank_nw"
   "cimpletoon/units/panther/0006", "u.panther_n", "u.tank_n"
   "cimpletoon/units/panther/0007", "u.panther_ne", "u.tank_ne"
   "cimpletoon/units/panther/0008", "u.panther_e", "u.tank_e"
 }
