
[spec]

; Format and options of this spec file:
options = "+Freeciv-3.1-spec"

[info]

artists = "
    Elefant
"

[extra]
sprites =
 { "file",                                 "tag"
   "cimpletoon/units/light_howitzer/0001", "u.light_howitzer_se", "u.spitter_se"
   "cimpletoon/units/light_howitzer/0002", "u.light_howitzer_s", "u.spitter_s"
   "cimpletoon/units/light_howitzer/0003", "u.light_howitzer_sw", "u.spitter_sw"
   "cimpletoon/units/light_howitzer/0004", "u.light_howitzer_w", "u.spitter_w"
   "cimpletoon/units/light_howitzer/0005", "u.light_howitzer_nw", "u.spitter_nw"
   "cimpletoon/units/light_howitzer/0006", "u.light_howitzer_n", "u.spitter_n"
   "cimpletoon/units/light_howitzer/0007", "u.light_howitzer_ne", "u.spitter_ne"
   "cimpletoon/units/light_howitzer/0008", "u.light_howitzer_e", "u.spitter_e"
 }
